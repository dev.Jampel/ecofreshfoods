package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type FoodItem struct {
	ID            string    `json:"id"`
	Name          string    `json:"name"`
	Origin        string    `json:"origin"`
	CurrentOwner  string    `json:"currentOwner"`
	Status        string    `json:"status"`
	DispatchDate  time.Time `json:"dispatchDate"`
	IsDistributor bool      `json:"isDistributor"`
	IsTransport   bool      `json:"isTransport"`
}

type EcoFreshFoodContract struct {
	contractapi.Contract
}

func (s *EcoFreshFoodContract) ApproveDistributor(ctx contractapi.TransactionContextInterface, id string) error {
	foodItem, err := s.ReadFoodItem(ctx, id)
	if err != nil {
		return err
	}

	foodItem.IsDistributor = true

	return s.UpdateFoodItem(ctx, foodItem.ID, foodItem.Name, foodItem.CurrentOwner, foodItem.Status)
}

func (s *EcoFreshFoodContract) ApproveTransport(ctx contractapi.TransactionContextInterface, id string) error {
	foodItem, err := s.ReadFoodItem(ctx, id)
	if err != nil {
		return err
	}

	foodItem.IsTransport = true

	return s.UpdateFoodItem(ctx, foodItem.ID, foodItem.Name, foodItem.CurrentOwner, foodItem.Status)
}

func (s *EcoFreshFoodContract) CreateFoodItem(ctx contractapi.TransactionContextInterface, id string, name string, origin string, currentOwner string, status string, dispatchDate time.Time, isDistributor bool, isTransport bool) error {
	// Ensure the initial owner is a farmer
	if len(id) == 0 || len(name) == 0 || len(origin) == 0 || len(currentOwner) == 0 || len(status) == 0 {
		return fmt.Errorf("all input fields must be non-empty")
	}

	foodItem := FoodItem{
		ID:            id,
		Name:          name,
		Origin:        origin,
		CurrentOwner:  currentOwner,
		Status:        status,
		DispatchDate:  dispatchDate,
		IsDistributor: isDistributor,
		IsTransport:   isTransport,
	}
	foodItemJSON, err := json.Marshal(foodItem)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(id, foodItemJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}

	return nil
}

func (s *EcoFreshFoodContract) ReadFoodItem(ctx contractapi.TransactionContextInterface, id string) (*FoodItem, error) {
	foodItemJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if foodItemJSON == nil {
		return nil, fmt.Errorf("the food item %s does not exist", id)
	}

	var foodItem FoodItem
	err = json.Unmarshal(foodItemJSON, &foodItem)
	if err != nil {
		return nil, err
	}

	return &foodItem, nil
}

func (s *EcoFreshFoodContract) UpdateFoodItem(ctx contractapi.TransactionContextInterface, id string, name string, currentOwner string, status string) error {
	foodItem, err := s.ReadFoodItem(ctx, id)
	if err != nil {
		return err
	}

	foodItem.Name = name
	foodItem.CurrentOwner = currentOwner
	foodItem.Status = status
	foodItem.DispatchDate = time.Now()

	foodItemJSON, err := json.Marshal(foodItem)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(id, foodItemJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state. %v", err)
	}

	eventPayload := fmt.Sprintf("Updated food item: %s", id)
	err = ctx.GetStub().SetEvent("UpdateFoodItem", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}

	return nil
}

func (s *EcoFreshFoodContract) DeleteFoodItem(ctx contractapi.TransactionContextInterface, id string) error {
	err := ctx.GetStub().DelState(id)
	if err != nil {
		return fmt.Errorf("failed to delete state: %v", err)
	}

	eventPayload := fmt.Sprintf("Deleted food item: %s", id)
	err = ctx.GetStub().SetEvent("DeleteFoodItem", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register. %v", err)
	}

	return nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(EcoFreshFoodContract))
	if err != nil {
		fmt.Printf("Error create food supply chain chaincode: %s", err.Error())
		return
	}

	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting food supply chain chaincode: %s", err.Error())
	}
}
