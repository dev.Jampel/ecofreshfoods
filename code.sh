PATH="/workspaces/ecofreshfood/bin:$PATH"

cryptogen generate --config=./crypto-config.yaml --output=crypto-config

# create genesis
configtxgen -outputBlock ./orderer/ecofreshfoodsgenesis.block -channelID ordererchannel -profile EcofreshfoodsOrdererGenesis
# create channeltx
configtxgen -outputCreateChannelTx ./ecofreshfoodschannel/ecofreshfoodschannel.tx -channelID ecofreshfoodschannel -profile EcofreshfoodsChannel


# Run tool bin to set peer
# Create Channel
peer channel create -c ecofreshfoodschannel -f ./config/ecofreshfoodschannel/ecofreshfoodschannel.tx --outputBlock ./config/ecofreshfoodschannel/ecofreshfoodschannel.block -o $ORDERER_ADDRESS

# join channel
peer channel join -b ./config/ecofreshfoodschannel/ecofreshfoodschannel.block -o $ORDERER_ADDRESS


# Run ecoff tool bin

# Package Chaincode
peer lifecycle chaincode package $CC_PACKAGE_FILE -p $CC_PATH --label $CC_LABEL
export GOFLAGS="-buildvcs=false"

# Install the package
peer lifecycle chaincode install $CC_PACKAGE_FILE

peer lifecycle chaincode queryinstalled


CC_PACKAGE_ID=foodmgt.1.0-1.0:4d605aee56252964db88faf8a7c678d7b8e0556c408157d2758e64b450f38b86

# Approve the chaincode
peer lifecycle chaincode approveformyorg -n foodmanagement -v 1.0 -C ecofreshfoodschannel --sequence 1 --package-id $CC_PACKAGE_ID

# Commit the chaincode
peer lifecycle chaincode checkcommitreadiness -n foodmanagement -v 1.0 -C ecofreshfoodschannel --sequence 1
peer lifecycle chaincode commit -n foodmanagement -v 1.0 -C ecofreshfoodschannel --sequence 1

# Verify the chaincode
peer lifecycle chaincode querycommitted -n foodmanagement -C ecofreshfoodschannel

# creating fooditem
peer chaincode invoke -C ecofreshfoodschannel -n foodmanagement -c '{"function":"CreateFoodItem","Args":["1", "Apple", "AppleFarm","FarmerSangay", "20 Box", "2000-01-01T00:00:00Z", "true","true"]}'

peer chaincode query -C ecofreshfoodschannel -n foodmanagement -c '{"function":"ReadFoodItem","Args":["1"]}'

echo $CC_VERSION 1.0

node wallet.js 
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
