EcoFreshFood Blockchain Network

Overview
The EcoFreshFood blockchain network leverages Hyperledger Fabric to provide a decentralized and secure platform for tracking the lifecycle of food items from farm to table. This README outlines the steps to set up the network, including deploying a Kafka cluster to ensure high availability, fault tolerance, and scalability for the ordering service.

Prerequisites
1. Docker
2. Docker Compose
3. Node.js
4. Go
5. Hyperledger Fabric binaries

Network Components

1. Hyperledger Fabric CA: For identity management and certificate issuance.
2. Orderer Nodes: Use Kafka as the consensus mechanism.
3. Peer Nodes: Hosted by different organizations to endorse transactions and maintain the ledger.
4. Kafka Cluster: Provides a fault-tolerant and scalable ordering service.
5. Zookeeper Ensemble: Manages the Kafka cluster.

Setup Instructions

1. Step 1: Clone the Repository
    - git clone https://github.com/your-repo/ecofreshfood-blockchain.git
    - cd ecofreshfood-blockchain
2. Step 2: Set Up the Kafka Cluster
    The Kafka cluster is managed using Docker Compose. Ensure that Docker and Docker Compose are installed on your machine.
    Navigate to the Kafka setup directory:
    cd kafka-setup
    Start the Zookeeper ensemble and Kafka brokers:
    docker-compose up -d
3. Step 3: Set Up Hyperledger Fabric Network
Generate the required crypto material and channel artifacts:

    - cd ../fabric-network
    - ./generateArtifacts.sh
    - Start the Fabric network:
    docker-compose up -d

4. Step 4: Deploy the Chaincode
    Install and instantiate the chaincode on peer nodes:

    ./deployChaincode.sh

5. Step 5: Set Up the REST API
    Navigate to the REST API directory:

    - cd ../rest-api
    - Install the necessary dependencies:
    - npm install
    - Start the REST API server:
    - npm start
6. Step 6: Access the Frontend Application
    Navigate to the frontend directory:
    cd ../frontend
    Open index.html in your web browser to access the application.

API Endpoints
The REST API provides the following endpoints to interact with the blockchain network:

    POST /foods: Create a new food item.
    GET /foods/:id: Read a food item by ID.
    PUT /foods/:id: Update an existing food item.
    DELETE /foods/:id: Delete a food item.
    Example Request (Create Food Item)
    json
    Copy code
    {
    "id": "4",
    "name": "Banana",
    "origin": "EcoFarm",
    "currentOwner": "Farmer Sangay",
    "status": "Available",
    "dispatchDate": "2024-05-25",
    "isDistributor": false,
    "isTransport": true
    }
    
Troubleshooting
Ensure all Docker containers are running:

    docker ps
    
Check the logs of a specific container for errors:

    docker logs <container_id>
    Contributions
    Contributions are welcome! Please fork the repository and submit a pull request for any enhancements or bug fixes.

License
    This project is licensed under the MIT License.