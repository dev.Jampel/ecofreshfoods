#!/bin/bash
export ORG_CONTEXT=EcofreshfarmerMSP
export ORG_NAME=EcofreshfarmerMSP
export CORE_PEER_LOCALMSPID=EcofreshfarmerMSP
# Logging specifications
export FABRIC_LOGGING_SPEC=INFO
# Location of the core.yaml
export FABRIC_CFG_PATH=/workspaces/ecofreshfood/config/eff
# Address of the peer
export CORE_PEER_ADDRESS=eff:7051
# Local MSP for the admin - Commands need to be executed as org admin
export CORE_PEER_MSPCONFIGPATH=/workspaces/ecofreshfood/config/crypto-config/peerOrganizations/ecofreshfarmer.com/users/Admin@ecofreshfarmer.com/msp
# Address of the orderer
export ORDERER_ADDRESS=orderer.ecofreshfoods.com:7050
export CORE_PEER_TLS_ENABLED=false
#### Chaincode related properties
export CC_NAME="foodmgt"
export CC_PATH="./chaincodes/foodmgt/"
export CC_CHANNEL_ID="ecofreshfoodschannel"
export CC_LANGUAGE="golang"
# Properties of Chaincode
export INTERNAL_DEV_VERSION="1.0"
export CC_VERSION="1.0"
export CC2_PACKAGE_FOLDER="./chaincodes/packages/"
export CC2_SEQUENCE=1
export CC2_INIT_REQUIRED="--init-required"
# Create the package with this name
export CC_PACKAGE_FILE="$CC2_PACKAGE_FOLDER$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION.tar.gz"
# Extracts the package ID for the installed chaincode
export CC_LABEL="$CC_NAME.$CC_VERSION-$INTERNAL_DEV_VERSION"
peer channel list




# {
#     "name": "EcofreshFoodNetwork",
#     "version": "1.0.0",
#     "client": {
#       "organization": "EcofreshfarmerMSP",
#       "connection": {
#         "timeout": {
#           "peer": {
#             "endorser": "300",
#             "eventHub": "300",
#             "eventReg": "300"
#           },
#           "orderer": "300"
#         }
#       }
#     },
#     "organizations": {
#       "EcofreshfarmerMSP": {
#         "mspid": "EcofreshfarmerMSP",
#         "peers": [
#           "peer0.ecofreshfarmer.com"
#         ],
#         "certificateAuthorities": [
#           "ca.ecofreshfarmer.com"
#         ]
#       }
#     },
#     "orderers": {
#       "orderer.ecofreshfoods.com": {
#         "url": "grpc://orderer.ecofreshfoods.com:7050"
#       }
#     },
#     "peers": {
#       "peer0.ecofreshfarmer.com": {
#         "url": "grpc://eff:7051",
#         "grpcOptions": {
#           "ssl-target-name-override": "peer0.ecofreshfarmer.com",
#           "hostnameOverride": "peer0.ecofreshfarmer.com"
#         },
#         "tlsCACerts": {
#           "pem": ""
#         }
#       }
#     },
#     "certificateAuthorities": {
#       "ca.ecofreshfarmer.com": {
#         "url": "http://ca.ecofreshfarmer.com:7054",
#         "caName": "ca-ecofreshfarmer",
#         "httpOptions": {
#           "verify": false
#         }
#       }
#     },
#     "channels": {
#       "ecofreshfoodschannel": {
#         "orderers": [
#           "orderer.ecofreshfoods.com"
#         ],
#         "peers": {
#           "peer0.ecofreshfarmer.com": {
#             "endorsingPeer": true,
#             "chaincodeQuery": true,
#             "ledgerQuery": true,
#             "eventSource": true
#           }
#         }
#       }
#     }
#   }
  

{
    "name": "EcoFreshFoodConsortium",
    "version": "1.0.0",
    "channels": {
        "ecofreshfoodschannel": {
            "orderers": [
                "orderer.ecofreshfoods.com"
            ],
            "peers": {
                "eff": {}
            }
        }
    },
    "organizations": {
        "Ecofreshfarmer": {
            "mspid": "EcofreshfarmerMSP",
            "peers": [
                "eff"
            ]
        }
    },
    "orderers": {
        "ecofreshfood-orderer": {
            "url": "grpc://orderer.ecofreshfoods.com:7050",
            "grpcOptions": {
                "ssl-target-name-override": "orderer.ecofreshfoods.com"
            }
        }
    },
    "peers": {
        "eff": {
            "url": "grpc://eff:7051"
        }
    }
}