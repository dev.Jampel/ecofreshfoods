document.getElementById('createForm').addEventListener('submit', async function (e) {
    e.preventDefault();
    const formData = new FormData(this);
    const data = {};
    formData.forEach((value, key) => {
        if (key === 'isDistributor' || key === 'isTransport') {
            data[key] = value === 'on';
        } else {
            data[key] = value;
        }
    });

    try {
        await axios.post('/foods', data);
        alert('Food item created successfully');
    } catch (error) {
        handleError(error);
    }
});

document.getElementById('readForm').addEventListener('submit', async function (e) {
    e.preventDefault();
    const id = document.getElementById('read-id').value;

    try {
        const response = await axios.get(`/foods/${id}`);

        // Customizing the display of response data
        const foodItem = response.data;
        const formattedResult = `
        <div class="flex items-center justify-center p-5">
            <div class="bg-white rounded-lg shadow-lg p-4">
                <p><strong>ID:</strong> ${foodItem.id}</p>
                <p><strong>Name:</strong> ${foodItem.name}</p>
                <p><strong>Origin:</strong> ${foodItem.origin}</p>
                <p><strong>Current Owner:</strong> ${foodItem.currentOwner}</p>
                <p><strong>Status:</strong> ${foodItem.status}</p>
                <p><strong>Dispatch Date:</strong> ${foodItem.dispatchDate}</p>
                <p><strong>Is Distributor:</strong> ${foodItem.isDistributor}</p>
                <p><strong>Is Transport:</strong> ${foodItem.isTransport}</p>
            </div>
        </div>
    
        `;

        // Setting the formatted result as the inner HTML of an element
        document.getElementById('read-result').innerHTML = formattedResult;
    } catch (error) {
        handleError(error);
    }
});



document.getElementById('updateForm').addEventListener('submit', async function (e) {
    e.preventDefault();
    const id = document.getElementById('update-id').value;
    const formData = new FormData(this);
    const data = {};
    formData.forEach((value, key) => {
        data[key] = value;
    });

    try {
        await axios.put(`/foods/${id}`, data);
        alert('Food item updated successfully');
    } catch (error) {
        handleError(error);
    }
});

document.getElementById('deleteForm').addEventListener('submit', async function (e) {
    e.preventDefault();
    const id = document.getElementById('delete-id').value;

    try {
        await axios.delete(`/foods/${id}`);
        alert('Food item deleted successfully');
    } catch (error) {
        handleError(error);
    }
});

function handleError(error) {
    if (error.response) {
        alert(`Error: ${error.response.data}`);
    } else if (error.request) {
        alert('Error: No response received from the server');
    } else {
        alert(`Error: ${error.message}`);
    }
}
