const express = require('express');
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');

const app = express();
app.use(express.json());
app.use(express.static(path.join(__dirname, 'public')));

app.post('/foods', async (req, res) => {
    try {
        const { id, name, origin, currentOwner, status, dispatchDate, isDistributor, isTransport } = req.body;

        // Check for missing parameters
        if (!id || !name || !origin || !currentOwner || !status || !dispatchDate || !isDistributor || !isTransport) {
            throw new Error('Missing required parameters');
        }

        // Proceed with the transaction
        const result = await submitTransaction('CreateFoodItem', id, name, origin, currentOwner, status, dispatchDate, isDistributor, isTransport);
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

app.get('/foods/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const result = await evaluateTransaction('ReadFoodItem', id);
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(404).send(`Failed to evaluate transaction: ${error}`);
    }
});

app.put('/foods/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const { name, currentOwner, status } = req.body;

        // Check for missing parameters
        if (!name || !currentOwner || !status) {
            throw new Error('Missing required parameters');
        }

        // Proceed with the transaction
        const result = await submitTransaction('UpdateFoodItem', id, name, currentOwner, status);
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

app.delete('/foods/:id', async (req, res) => {
    try {
        const { id } = req.params;

        // Proceed with the transaction
        const result = await submitTransaction('DeleteFoodItem', id);
        res.send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

async function getContract() {
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const identity = await wallet.get('Admin@ecofreshfarmer.com');

    if (!identity) {
        throw new Error('Admin identity not found in wallet');
    }

    const gateway = new Gateway();
    const connectionProfile = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'connection.json'), 'utf8'));
    const connectionOptions = { wallet, identity: 'Admin@ecofreshfarmer.com', discovery: { enabled: false, asLocalhost: true } };

    await gateway.connect(connectionProfile, connectionOptions);

    const network = await gateway.getNetwork('ecofreshfoodschannel');
    const contract = network.getContract('foodmanagement');

    return contract;
}

async function submitTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.submitTransaction(functionName, ...args);
    return result.toString();
}

async function evaluateTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.evaluateTransaction(functionName, ...args);
    return result.toString();
}

app.get('/', (req, res) => {
    res.send('Hello, World!');
});

module.exports = app;
